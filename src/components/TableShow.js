import "antd/dist/antd.css";
import React, { Component } from "react";
import { Table } from "antd";
import Axois from "axios";

const columns = [
  { title: "ID", dataIndex: "id", key: "id" },
  { title: "ApiEnv", dataIndex: "apiEnv", key: "apiEnv" },
  { title: "Key", dataIndex: "key", key: "keyvalue" },
  { title: "CC", dataIndex: "cc", key: "cc" },
  { title: "LangId", dataIndex: "langId", key: "langId" },
  //   { title: "fv", dataIndex: "fv", key: "fv" },
  { title: "theme", dataIndex: "theme", key: "theme" },
  { title: "isDefault", dataIndex: "isDefault", key: "isDefault" },
  { title: "Created", dataIndex: "created", key: "created" },
  { title: "Updated", dataIndex: "updated", key: "updated" },
  //   {
  //     title: "Action",
  //     dataIndex: "",
  //     key: "x",
  //     render: () => <a>Delete</a>,
  //   },
];

class TableShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataArray: [],
      errorMsg: "",
    };
  }

  componentDidMount() {
    const url = this.props.url;
    console.log(url);
    Axois.get(url)
      .then((response) => {
        console.log(response.data);

        this.setState({ dataArray: response.data });
      })
      .catch((error) => {
        this.setState({
          errorMsg: "Data retreiving eroor",
        });
      });
  }

  render() {
    //record.fv;
    // console.log(this.state.dataArray);
    return (
      <Table
        columns={columns}
        expandable={{
          expandedRowRender: (record) => (
            <p style={{ margin: 0 }}>
              {JSON.stringify(record.rootTrans, null, 2)}
            </p>
          ),
          rowExpandable: (record) => record.fv !== "Not Expandable",
        }}
        dataSource={this.state.dataArray}
      />
    );
  }
}
export default TableShow;

// const data = [
//   {
//     key: 1,
//     name: "John Brown",
//     age: 32,
//     address: "New York No. 1 Lake Park",
//     description:
//       "My name is John Brown, I am 32 years old, living in New York No. 1 Lake Park.",
//   },
//   {
//     key: 2,
//     name: "Jim Green",
//     age: 42,
//     address: "London No. 1 Lake Park",
//     description:
//       "My name is Jim Green, I am 42 years old, living in London No. 1 Lake Park.",
//   },
//   {
//     key: 3,
//     name: "Not Expandable",
//     age: 29,
//     address: "Jiangsu No. 1 Lake Park",
//     description: "This not expandable",
//   },
//   {
//     key: 4,
//     name: "Joe Black",
//     age: 32,
//     address: "Sidney No. 1 Lake Park",
//     description:
//       "My name is Joe Black, I am 32 years old, living in Sidney No. 1 Lake Park.",
//   },
// ];

// ReactDOM.render(
//   <Table
//     columns={columns}
//     expandable={{
//       expandedRowRender: (record) => (
//         <p style={{ margin: 0 }}>{record.rootTrans}</p>
//       ),
//       rowExpandable: (record) => record.key !== "Not Expandable",
//     }}
//     dataSource={data}
//   />,
//   document.getElementById("container")
// );
