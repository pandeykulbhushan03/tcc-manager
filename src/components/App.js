import React from "react";
import "../Styles/App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Sidebar from "./Sidebar";

const App = () => {
  return (
    <React.Fragment>
      <Sidebar />
    </React.Fragment>
  );
};

export default App;
