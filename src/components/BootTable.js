import React, { Component } from "react";
import * as ReactBootstrap from "react-bootstrap";
import Axois from "axios";

class BootTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DataArray: [],
      errorMsg: "",
    };
  }

  componentDidMount() {
    const url = this.props.url;
    console.log(url);
    Axois.get(url)
      .then((response) => {
        console.log(response.data);

        this.setState({ DataArray: response.data });
      })
      .catch((error) => {
        this.setState({
          errorMsg: "Data retreiving eroor",
        });
      });
  }

  format(datas) {
    var result = "[";
    datas.forEach((element) => {
      result += element + ", ";
    });

    result = result.substring(0, result.length - 2) + "]";

    return <p>{result}</p>;
  }

  render() {
    return this.errorMsg ? (
      <h1 className="error">Data retreiving error</h1>
    ) : (
      <ReactBootstrap.Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Created</th>
            <th>Updated</th>
            <th>apiEnv</th>
            <th>Key</th>
            <th>CC</th>
            <th>langID</th>
            <th>fv</th>
            <th>theme</th>
            <th>isDefault</th>
          </tr>
        </thead>
        <tbody>
          {this.state.DataArray.map((data) => {
            return (
              <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.created}</td>
                <td>{data.updated}</td>
                <td>{data.apiEnv}</td>
                <td>{data.key}</td>
                <td>{data.cc === 0 ? " " : data.cc}</td>
                <td>{data.langId === 0 ? " " : this.format(data.langId)}</td>
                <td>{data.fv}</td>
                <td>{data.theme === 0 ? " " : this.format(data.theme)}</td>
                <td>{data.isDefault ? "true" : "false"}</td>
              </tr>
            );
          })}
        </tbody>
      </ReactBootstrap.Table>
    );
  }
}

export default BootTable;
