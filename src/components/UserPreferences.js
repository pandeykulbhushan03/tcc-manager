import React, { Component } from "react";
import * as ReactBootstrap from "react-bootstrap";
import BootTable from "./BootTable";
import TableShow from "./TableShow";

class UserPreferences extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiEnv: "toia",
      key: "",
      fv_from: 0,
      fv_to: 0,
      url: "",
    };
  }

  handleKeyChange = (event) => {
    this.setState({
      url: "",
      key: event.target.value,
    });
  };

  handleFvFromChange = (event) => {
    this.setState({
      url: "",
      fv_from: event.target.value,
    });
  };

  handleFvToChange = (event) => {
    this.setState({
      url: "",
      fv_to: event.target.value,
    });
  };

  handleApiEnvChange = (event) => {
    this.setState({
      url: "",
      apiEnv: event.target.value,
    });
  };

  onSubmitHandler = (event) => {
    event.preventDefault();

    const baseUrl =
      "https://nprelease.indiatimes.com/tcc-manager/config/moderate/";
    const newUrl = `${baseUrl}${this.state.apiEnv}/trans/${this.state.key}?fv=${this.state.fv_from}&endFv=${this.state.fv_to}`;

    // console.log(newUrl);

    this.setState({
      url: newUrl,
    });
  };

  render() {
    return (
      <div className="userPreferences">
        <h5>Filters</h5>
        <ReactBootstrap.Table hover responsive="md">
          <tbody>
            <tr>
              <td>
                <label htmlFor="apiEnv">API Environment</label>
              </td>
              <td>
                <div className="selectWrapper">
                  <select
                    className="selectBox"
                    name="apiEnv"
                    id="apiEnv"
                    value={this.state.apiEnv}
                    onChange={this.handleApiEnvChange}
                  >
                    <option value="toia">TOIA</option>
                    <option value="toii">TOII</option>
                    <option value="toim">TOIM</option>
                    <option value="toiw">TOIW</option>
                  </select>
                </div>
              </td>
              <td>
                <label htmlFor="key">Key</label>
              </td>
              <td>
                <input
                  type="text"
                  name="key"
                  id="key"
                  value={this.state.key}
                  placeholder="Type here your key"
                  onChange={this.handleKeyChange}
                />
              </td>
              <td>
                <label htmlFor="">FV From</label>
              </td>
              <td>
                <input
                  type="number"
                  name="fv_from"
                  value={this.state.fv_from}
                  id="fv_from"
                  placeholder="from"
                  onChange={this.handleFvFromChange}
                />
              </td>
              <td>
                <label htmlFor="">FV To</label>
              </td>
              <td>
                <input
                  type="number"
                  name="fv_to"
                  value={this.state.fv_to}
                  id="fv_to"
                  placeholder="to"
                  onChange={this.handleFvToChange}
                />
              </td>
            </tr>
          </tbody>
        </ReactBootstrap.Table>

        <ReactBootstrap.Button
          type="button"
          className="submitBtn"
          onClick={this.onSubmitHandler}
        >
          Submit
        </ReactBootstrap.Button>
        {/* {this.state.url.length !== 0 ? <BootTable url={this.state.url} /> : ""} */}
        <div>
          {this.state.url.length !== 0 ? (
            <TableShow url={this.state.url} />
          ) : (
            // <BootTable url={this.state.url} />
            ""
          )}
        </div>
      </div>
    );
  }
}

export default UserPreferences;
